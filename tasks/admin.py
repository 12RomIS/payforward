# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from tasks.models import *


# Register your models here.
admin.site.register(Task)
admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Status)
