# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseBadRequest
from django.template import RequestContext
from django.shortcuts import render, render_to_response, redirect
from tasks.models import *
from forms import *
# Create your views here.


def index(request):
    tasks = Task.objects.all()
    important_tasks = tasks.order_by('-rate')[0:3]
    fresh_tasks = tasks.order_by('-create_date')[0:3]
    form = CreateTaskForm()
    return render_to_response('index.html', {
        'important_tasks': important_tasks,
        'fresh_tasks': fresh_tasks,
        'task_form': form
    })


def create_task(request):
    if request.method == 'POST':
        form = CreateTaskForm(request.POST, request.FILES)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.save()
            return redirect('/task/' + str(task.id))
    return HttpResponseBadRequest()


def tasks(request):
    tasks = Task.objects.all()
    return render_to_response('tasks.html', {'tasks': tasks})


def task(request, task_id):
    tasks = Task.objects.exclude(id=task_id)[0:3]
    task = Task.objects.get(id=task_id)
    return render_to_response('task.html', {
        'tasks': tasks,
        'task': task
    })


def people(request):
    return render_to_response('people.html')


def profile(request, user_id):
    return render_to_response('profile.html')