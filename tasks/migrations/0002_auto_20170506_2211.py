# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-06 19:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tag',
            options={'verbose_name': '\u0422\u0435\u0433', 'verbose_name_plural': '\u0442\u0435\u0433\u0438'},
        ),
        migrations.AddField(
            model_name='task',
            name='create_date',
            field=models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='task',
            name='end_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='\u0421\u0440\u043e\u043a\u0438 \u0437\u0430\u0434\u0430\u0447\u0438'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='name',
            field=models.CharField(max_length=50, verbose_name='\u0418\u043c\u044f \u0422\u0435\u0433\u0430 \u0442\u0440\u0430\u043d\u0441\u043b\u0438\u0442\u043e\u043c'),
        ),
        migrations.AlterField(
            model_name='task',
            name='tags',
            field=models.ManyToManyField(related_name='tasks', to='tasks.Tag', verbose_name='\u0442\u0435\u0433\u0438'),
        ),
    ]
