# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Category (models.Model):
    name = models.CharField(max_length=50, verbose_name='Имя категории транслитом')
    title = models.CharField(max_length=50, verbose_name='Название категории')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'категории'

    def __unicode__(self):
        return self.title


class Tag (models.Model):
    name = models.CharField(max_length=50, verbose_name='Имя Тега транслитом')
    title = models.CharField(max_length=50, verbose_name='Название тега')

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'теги'

    def __unicode__(self):
        return self.title


class Status (models.Model):
    name = models.CharField(max_length=255, verbose_name='Статус выполнения задачи')

    class Meta:
        verbose_name = 'Статусы'
        verbose_name_plural = 'статус'

    def __unicode__(self):
        return self.name


class Task (models.Model):
    user = models.ForeignKey(User, related_name='tasks', verbose_name='Автор')
    title = models.CharField(max_length=100, verbose_name='Название задачи')
    description = models.TextField()
    category = models.ForeignKey(Category, related_name='tasks', verbose_name='Категория')
    # tags = models.ManyToManyField(Tag, related_name='tasks', verbose_name=u'теги')
    create_date = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    end_date = models.DateTimeField(blank=True, null=True, verbose_name='Сроки задачи')
    rate = models.IntegerField(verbose_name='Рейтинг', null=True)
    task_photo = models.ImageField(upload_to='task_photos', verbose_name='Фото', default='task_photos/image.jpg', blank=True, null=True)
    status = models.ForeignKey(Status)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'задачи'

    def __unicode__(self):
        return self.title


